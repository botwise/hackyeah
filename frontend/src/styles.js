import styled from 'styled-components'
import TextArea from './components/TextArea/TextArea'

export const MainWrapper = styled.div`
    height: 100vh;
    max-height: 100vh;
    width: 100vw;
    max-width: 1080px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`

export const TextAreaWrapper = styled(TextArea)`
    height: 15vh;
`

export const ChatArea = styled.div`
    width: 100%;
    height: 85%;
    max-height: 85%;
    overflow: auto;
`