import React from 'react';
import TextArea from './components/TextArea/TextArea.js';
import GlobalStyle from './assets/styles/GlobalStyle';
import { MainWrapper, ChatArea } from './styles.js';
import Chat from './components/Chat/Chat.js';


function App() {
  return (
    <MainWrapper>
      <GlobalStyle />
      <TextArea />
      <ChatArea>
        <Chat />
      </ChatArea>
    </MainWrapper>
  );
}

export default App;
