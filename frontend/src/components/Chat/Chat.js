import React, { Component } from 'react';
import { ChatWrapper } from './style';
import ChatPiece from '../ChatPiece/ChatPiece';
import { connect } from 'react-redux'



class Chat extends Component {
    constructor(props) {
        super()
        this.state = {
            list: props.list ? props.list : []
        }
    }

    render() {

        const ChatList = this.props.list.map(item => <ChatPiece key={item.id} text={item.text} type={item.type}/>)
        return (
            <ChatWrapper onClick={console.log(this.props.list)}>
                {ChatList.reverse()}
            </ChatWrapper>
        )
    }
}


const mapStateToProps = (state) => {
    return{
        list: state.list,
    }
}
  export default connect(mapStateToProps)(Chat)