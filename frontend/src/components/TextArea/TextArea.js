import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { TextAreaWrapper, ButtonArea, LoaderWrapper, Loader } from './styles';
import Button from '@material-ui/core/Button';
import {connect} from 'react-redux'
import axios from 'axios'
import debounce from 'lodash.debounce'
import EULoader from '../../assets/img/EULoader.png'

const useStyles = makeStyles(theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
    dense: {
      marginTop: 19,
    },
    menu: {
      width: 200,
    },
  }));

class TextArea extends React.Component {

  constructor() {
    super()
    this.state = {
      value: '',
      list: [],
      isLoading: false,
    }
  }

    postMsg = () => {
      if (this.state.value !== '') {
        this.setState({value: ''})
        this.setState({isLoading: true})
        this.setState({list: this.state.list.concat({
          text: this.state.value,
          id: this.state.list.length,
          type: 'Q'
        })})

        this.sendData()
        console.log(this.state.list);
        this.sendData()
        console.log(this.state.list);
        axios.post('/message', {
          text: this.state.value,
        })
        .then(response => {
          console.log(response);
          console.log(response.data.text);
          this.setState({list: this.state.list.concat({
            text: response.data.text,
            id: this.state.list.length,
            type: 'A'
          })})
          this.setState({isLoading: false})
        })
        .catch(error => {
          console.log(error);
        })

        console.log('POST');
      }
    }

    sendData = () => {
      this.props.dispatch({
        type: 'VALUE',
        payload: this.state.value
      })
      this.props.dispatch({
        type: 'LIST',
        payload: this.state.list
      })
    }

    componentDidUpdate() {
      this.sendData()
    }

    handleClickDown = e => {
      if (e.key === 'Enter') {
        this.postMsg();
        console.log('enter key pressed')
      }
    }

  render() {
    return (
        <TextAreaWrapper>
            <TextField
                    id="standard-full-width"
                    label="Type your question here:"
                    style={{ margin: 8 }}
                    placeholder="What would you like to know about the EU?"
                    fullWidth
                    margin="normal"
                    InputLabelProps={{
                    shrink: true,
                    }}
                    onChange={(e) => this.setState({
                      value: e.target.value,
                    })}
                    onKeyPress={this.handleClickDown}
                    value={this.state.value}

                />
                <ButtonArea>
                  <Button onClick={this.postMsg} variant="contained" color="primary" >
                    Search
                  </Button>
                </ButtonArea>
                <LoaderWrapper isLoading={this.state.isLoading}>
                  <Loader src={EULoader} alt="EULoader"/>
                </LoaderWrapper>
        </TextAreaWrapper>

    )
  }
}
const mapStateToProps = (state) => {
  return{
      value: state.value,
      list: state.list,
  }
}
export default connect(mapStateToProps)(TextArea)
