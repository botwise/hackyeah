const proxy = require('http-proxy-middleware');

const proxyTarget = 'https://eu.botwise.io';

console.log('proxyTarget ', proxyTarget);
    module.exports = function(app) {
    app.use(proxy('/message', {
        target: proxyTarget,
        changeOrigin: true,
        secure: false
    }));
};