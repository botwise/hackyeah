import styled from 'styled-components'
import { lightGreen } from '@material-ui/core/colors'

export const ChatPieceWrapper = styled.div`
    padding: 5px 10px;
    margin: 5px;
    border: none;
    border-radius: 20px;
    background-color: ${({type}) => type === 'Q' ? 'lightBlue' : 'lightGreen'};
    font-size: 40%;
`