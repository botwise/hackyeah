Chatbot API

Acceptance criteria:
- uses chosen model (elasticsearch or ml)
- client sends question and gets answer in response
- client sends metadata (e.g. name) used to personalize answers
