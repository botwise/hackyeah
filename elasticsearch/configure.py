#!/usr/bin/env python3

from elasticsearch import Elasticsearch


def configure_chatbot_index(es):
    index_name = "chatbot"
    es.indices.create(index=index_name, ignore=400)

    data_mapping = {
        "properties": {
            "question": {
                "type": "text",
                "analyzer": "english",
                "fields": {
                   "raw": {
                       "type": "keyword"
                   }
                }
            },
            "context": {
                "type": "text",
                "analyzer": "english"
            },
            "answer": {
                "type": "text",
                "analyzer": "english"
            }
        }
    }

    es.indices.put_mapping(index=index_name, body=data_mapping)


if __name__ == "__main__":
    client = Elasticsearch()

    configure_chatbot_index(client)
