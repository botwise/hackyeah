from profanity_check import predict, predict_prob

PROFANITY_TRESHOLD = 0.3

def check_profanity(question):
    profanity_prob = predict_prob([question])[0]
    return profanity_prob > PROFANITY_TRESHOLD
