#!/usr/bin/env python3

import json
from time import sleep
from elasticsearch import Elasticsearch
import pandas as pd
import json

index_name = "chatbot"


def load_json(filename):
    docs = []
    with open(filename, 'r') as f:
        loaded_json = json.load(f)
        for hit in loaded_json['hits']['hits']:
            source = hit["_source"]
            answer = source['answer'] if 'answer' in source else ""
            context = source['context'] if 'context' in source else ""
            question = source['question'] if 'question' in source else ""
            docs.append({
                "answer": answer,
                "context": context,
                "question": question
            })
    return docs

def load_csv(filename):
    d = pd.read_csv(filename)
    d.columns = ['question', 'answer']
    d['context'] = ""
    return json.loads(d.to_json(orient='records'))


def upload(es, docs):
    for doc in docs:
        res = es.index(index=index_name, body=doc)
        sleep(0.005)
    
    print(res['result'])

    es.indices.refresh(index=index_name)


if __name__ == "__main__":
    client = Elasticsearch()

    # docs = load_json("default.json")
    # upload(client, docs)
    docs = load_csv('qa.csv')
    upload(client, docs)