#!/usr/bin/env python3

from elasticsearch import Elasticsearch

index_name = "chatbot"

class ElasticsearchModel:

    def __init__(self, host="elasticsearch", port=9200):
        self.es = Elasticsearch(host, port=port)

    def answer_question(self, message):
        body = {
        	"size": 1,
            "query": {
                "multi_match": {
                    "query": message,
                    "fields": ["question^10", "answer", "context"]
        		}
            }
        }

        res = self.es.search(index=index_name, body=body)
        hits = res['hits']
        if 'hits' in hits:
            found_hits = hits['hits']
            if len(found_hits) > 0:
                return found_hits[0]["_source"]["answer"]

        return None


if __name__ == "__main__":

    model = ElasticsearchModel()
    answer = model.answer_question("what is life about?")

    print(answer)
