import styled from 'styled-components'

export const TextAreaWrapper = styled.div`
    width: 95%;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const ButtonArea = styled.div`
    height: 30px;
`

export const LoaderWrapper = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: rgba(255,255,255,.4);
    display: flex;
    justify-content: center;
    align-items: center;
    display: ${props => props.isLoading ? 1 : 'none'};
`

export const Loader = styled.img`
    width: 60%;
    animation-name: spin;
    animation-duration: 5000ms;
    animation-iteration-count: infinite;
    animation-timing-function: linear;

    @keyframes spin {
    from {
        transform:rotate(0deg);
    }
    to {
        transform:rotate(360deg);
    }
}
`