import {createGlobalStyle} from 'styled-components'

const GlobalStyle = createGlobalStyle`
html {
    font-size: 100%;
}

body {
    width: 100vw;
    height: 100vh;
    padding: 0;
    margin: 0;
    font-family: 'Montserrat', sans-serif;
    font-size: 16px;
    font-size: 1.6rem;
    line-height: 1.5;
    display: flex;
    flex-direction: column;
    align-items: center;
}

*, *::before, *::after {
    box-sizing: border-box;
}

button {
    background: none;
    border: none;
    font-family: inherit;
    padding: 0;
}
`

export default GlobalStyle