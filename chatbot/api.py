#!flask/bin/python
from flask import Flask, jsonify, request, abort
from es_model import ElasticsearchModel
from sentiment_models import check_profanity
import random

app = Flask(__name__)
model = ElasticsearchModel()

ABUSIVE_QUESTION = ["I think that's not appropriate language. Please be respectful :)",
                    "Does your mother know that you are talking like this?",
                    "I don't know how to respond to his text",
                    "I think it's rude."]

ANSWER_NOT_FOUND = ["I don’t know this but I can answer your question about the Brexit.",
                    "This is out of my knowledge base. Please, ask me something about Brexit."]

@app.route('/health')
def health():
    return "UP"


@app.route('/message', methods=['POST'])
def message():
    question = None
    if request.is_json:
        json = request.get_json()
        if 'text' in json:
            question = json['text']
            if check_profanity(question):
                return jsonify({'text': random.choice(ABUSIVE_QUESTION)})
            answer = model.answer_question(question)
            if answer is None:
                answer = random.choice(ANSWER_NOT_FOUND)
            return jsonify({'text': answer})
    return abort(400)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8080)
