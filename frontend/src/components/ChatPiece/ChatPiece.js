import React from 'react';
import styled from 'styled-components'

const ChatPieceWrapper = styled.div`
    padding: 5px 10px;
    margin: 5px;
    border: none;
    border-radius: 20px;
    background-color: ${props => props.type === 'A' ? 'lightBlue' : 'lightGrey'};
    font-size: 67.5%;
`

const ChatPiece = props => {
    return(
        <ChatPieceWrapper type={props.type}><p dangerouslySetInnerHTML={{__html: props.text}} /></ChatPieceWrapper>
    )
}
export default ChatPiece